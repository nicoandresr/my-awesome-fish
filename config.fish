#
#  FFFFFFFFFFFFFFFFFFFFFFIIIIIIIIII   SSSSSSSSSSSSSSS HHHHHHHHH     HHHHHHHHH
#  F::::::::::::::::::::FI::::::::I SS:::::::::::::::SH:::::::H     H:::::::H
#  F::::::::::::::::::::FI::::::::IS:::::SSSSSS::::::SH:::::::H     H:::::::H
#  FF::::::FFFFFFFFF::::FII::::::IIS:::::S     SSSSSSSHH::::::H     H::::::HH
#    F:::::F       FFFFFF  I::::I  S:::::S              H:::::H     H:::::H
#    F:::::F               I::::I  S:::::S              H:::::H     H:::::H
#    F::::::FFFFFFFFFF     I::::I   S::::SSSS           H::::::HHHHH::::::H
#    F:::::::::::::::F     I::::I    SS::::::SSSSS      H:::::::::::::::::H
#    F:::::::::::::::F     I::::I      SSS::::::::SS    H:::::::::::::::::H
#    F::::::FFFFFFFFFF     I::::I         SSSSSS::::S   H::::::HHHHH::::::H
#    F:::::F               I::::I              S:::::S  H:::::H     H:::::H
#    F:::::F               I::::I              S:::::S  H:::::H     H:::::H
#  FF:::::::FF           II::::::IISSSSSSS     S:::::SHH::::::H     H::::::HH
#  F::::::::FF           I::::::::IS::::::SSSSSS:::::SH:::::::H     H:::::::H
#  F::::::::FF           I::::::::IS:::::::::::::::SS H:::::::H     H:::::::H
#  FFFFFFFFFFF           IIIIIIIIII SSSSSSSSSSSSSSS   HHHHHHHHH     HHHHHHHHH
#

# Set the editor to neovim
export VISUAL=nvim
export EDITOR="$VISUAL"
# Config powerline
set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"
powerline-setup
# Define alias
alias cat "bat" # bat must be istalled yay -S bat
alias n "nvim"
alias f 'firefox-developer-edition'
# Environments
set NPM_CONFIG_PREFIX "~/.npm-global"
fenv source ~/.nvm/nvm.sh
# fenv source ~/.nvm/nvm.sh \; nvm use 12
