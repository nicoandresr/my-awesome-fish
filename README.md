# Pre install
you must to install powerline.

## Install dependencies
Install Python3 via homebrew (Optional: in case you do not have pyhton installed. run python3 --version to see if it's installed)
```shell
$ brew install python3
```
Install powerline via pip (python package manager)
```shell
$ pip3 install powerline-status
```
Find the location
```shell
$ pip3 show powerline-status
```
It shows that the powerline-status is installed at
```shell
/usr/local/lib/python3.6/site-packages
```
# Install
Just clone this repo on your fish config path

```shell
git clone git@gitlab.com:nicoandresr/my-awesome-fish.git ~/.config/fish
```

# Config
Powerline change user name in path.
